using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Db
{
    public partial class Articles
    {
        [Key]
        [Column(Order = 0)]
        public Guid Id { get; set; }

        [Column(Order = 1)]
        [StringLength(150)]
        public string Title { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        [NotMapped]
        public float Score { get; set; }
    }
}
