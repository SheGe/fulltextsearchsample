namespace Db
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DbContext : System.Data.Entity.DbContext
    {
        public static string ConnectionString = @"data source=W-WROBEL2\SQLEXPRESS;initial catalog=FullText;integrated security=True";
        public DbContext()
            : base(ConnectionString)
        {
        }

        public virtual DbSet<Articles> Articles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Articles>()
                .Property(e => e.Id)
                .IsRequired();

            modelBuilder.Entity<Articles>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Articles>()
                .Property(e => e.Description)
                .IsUnicode(false);
        }
    }
}
