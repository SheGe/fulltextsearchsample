﻿using Db;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Xml;

namespace DbInserter
{
    class Program
    {
        static void Main(string[] args)
        {
            const string path = @"../../data.xml";

            var articles = GetArticleList(path).ToList();

            try
            {
                using (var transaction = new TransactionScope())
                {
                    ClearDbTable();
                    InsertArticlesToDb(articles);

                    transaction.Complete();
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Niepowodzenie: {0}", exc.Message);
            }
        }

        private static void ClearDbTable()
        {
            using (var connection = new SqlConnection(DbContext.ConnectionString))
            {
                connection.Open();

                using (var cmd = new SqlCommand("DELETE FROM Articles", connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private static void InsertArticlesToDb(List<Articles> articles)
        {
            using (var db = new DbContext())
            {
                for (int i = 0; i < articles.Count; i++)
                {
                    db.Articles.Add(articles[i]);
                    Console.WriteLine("Insert {0,4}/{1,4} | {2}", i + 1, articles.Count, articles[i].Title);
                }

                Console.WriteLine("Zapisywanie zmian w bazie...");
                db.SaveChanges();
                Console.WriteLine("Sukces!");
            }
        }

        private static IEnumerable<Articles> GetArticleList(string path)
        {
            var xml = new XmlDocument();

            xml.Load(path);

            var root = xml.SelectSingleNode("root");
            var events = root.SelectNodes("//Event");
            var places = root.SelectNodes("//Place");

            foreach (XmlNode @event in events)
            {
                var title = @event["title"].InnerText;
                var cuttedTitle = title.Length > 150 ? title.Substring(0, 149) : title;

                yield return new Articles
                {
                    Id = Guid.NewGuid(),
                    Title = cuttedTitle,
                    Description = @event["description"].InnerText
                };
            }

            foreach (XmlNode @place in places)
            {
                var name = @place["name"].InnerText;
                var cuttedName = name.Length > 150 ? name.Substring(0, 149) : name;

                yield return new Articles
                {
                    Id = Guid.NewGuid(),
                    Title = cuttedName,
                    Description = @place["description"].InnerText
                };
            }
        }
    }
}
