﻿using SearchEngine;
using SearchEngine.Shared;
using System.Web.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using Db;

namespace FullTextSearchWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Item(Guid id)
        {
            if (id == null)
                return RedirectToAction("Index");

            Articles article;

            using (var db = new DbContext())
            {
                article = db.Articles.Where(n => n.Id == id).First();
            }

            return View(article);
        }

        public JsonResult Search(string phrase, eSearcher searcherType)
        {
            var searcher = new Searcher(false);
            var searchResult = searcher.Search(phrase, searcherType);

            return Json(searchResult, JsonRequestBehavior.AllowGet);
        }
    }
}