﻿$(document).ready(() => ko.applyBindings(new SearchViewModel(), document.getElementById("search-container")));

class SearchViewModel {
    mainSearchInput: KnockoutObservable<string>;

    sqlSearchConfiguration: ISearchConfiguration;
    luceneSearchConfiguration: ISearchConfiguration;
    examineSearchConfiguration: ISearchConfiguration;

    phraseChange: (data, event) => void;
    mainPhraseChange: () => void;
    openArticle: (data) => void;

    constructor() {
        var self = this;

        self.mainSearchInput = ko.observable<string>();

        self.sqlSearchConfiguration = { type: eSearcher.Sql, phrase: ko.observable<string>(), articles: ko.observableArray<IArticle>(), executionTime: ko.observable(0) };
        self.luceneSearchConfiguration = { type: eSearcher.Lucene, phrase: ko.observable<string>(), articles: ko.observableArray<IArticle>(), executionTime: ko.observable(0) };
        self.examineSearchConfiguration = { type: eSearcher.Examine, phrase: ko.observable<string>(), articles: ko.observableArray<IArticle>(), executionTime: ko.observable(0) };

        self.phraseChange = function (data: ISearchConfiguration, event): void {
            if (data.phrase().length == 0) {
                data.phrase("");
                data.articles([]);
                data.executionTime(0);
            }
            else if (data.phrase().length > 3)
                $.ajax({
                    url: "/home/search",
                    data: {
                        phrase: data.phrase(),
                        searcherType: data.type
                    }
                }).then((result: ISearchResult) => {
                    data.articles(result.Articles);
                    data.executionTime(result.ExecutionTime);
                });
        }
        self.mainPhraseChange = function (): void {
            var update = (conf: ISearchConfiguration) => {
                conf.phrase(self.mainSearchInput());
                self.phraseChange(conf, null);
            };

            update(self.sqlSearchConfiguration);
            update(self.luceneSearchConfiguration);
        }
        self.openArticle = function (data: IArticle): void {
            var a: HTMLAnchorElement = document.createElement("a");

            a.href = "/home/item/" + data.Id;
            a.target = "_blank";
            a.click();
        }
    }
}

interface ISearchConfiguration {
    phrase: KnockoutObservable<string>;
    type: eSearcher;
    executionTime: KnockoutObservable<number>;
    articles: KnockoutObservableArray<IArticle>;
}

interface ISearchResult {
    ExecutionTime: number;
    Articles: Array<IArticle>;
}

interface IArticle {
    Id: string;
    Title: string;
    Description: string;
    Score: number;
}

enum eSearcher {
    Sql = 0, Lucene = 1, Examine = 2
}
