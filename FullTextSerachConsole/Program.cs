﻿using System;
using System.Linq;
using SearchEngine.LuceneNET;
using SearchEngine.Sql;
using SearchEngine.Shared;
using System.Threading.Tasks;

namespace FullTextSerachConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                PrintWelcome();

                var sqlSearcher = new SqlSearcher();
                var fullTextSearcher = new LuceneSearcher();
                
                var phrase = GetValueInlineCaption<string>("Wprowadź szukaną frazę");

                sqlSearcher.SearchCompleted += SearchCompleted;
                fullTextSearcher.SearchCompleted += SearchCompleted;

                Task.WaitAll(sqlSearcher.SearchAsync(phrase), fullTextSearcher.SearchAsync(phrase));
                
                Console.WriteLine("\nNaciśnij dowolny klawisz by wyszukać ponownie, ESC aby zakończyć");

                var key = Console.ReadKey();

                if (key.Key == ConsoleKey.Escape)
                    return;
                else
                    continue;
            }
        }

        public static T GetValueInlineCaption<T>(string caption)
        {
            string result = string.Empty;

            while (string.IsNullOrEmpty(result))
            {
                Console.Write("#{0}: ", caption);
                result = Console.ReadLine();
                Console.WriteLine();
            }

            return (T)Convert.ChangeType(result, typeof(T));
        }

        private static void SearchCompleted(SearchResult e)
        {
            var results = e.Articles.ToList();

            Console.Write("+----------------------------------+\n");
            Console.Write("|        {0,16}          |\n", e.SearcherName);
            Console.Write("+----------------------------------+\n");
            Console.Write("| Wyników: {0,3} | Wykonano w {1,4}ms |\n", results.Count, e.ExecutionTime);
            Console.Write("+----------------------------------+\n");

            results.ForEach(n =>
            {
                var title = n.Title.Length <= 32 ? n.Title : string.Concat(n.Title.Substring(0, 29), "...");
                Console.Write("+ {0,32} +\n", title);
            });

            if (results.Any())
                Console.Write("+----------------------------------+\n");
        }

        private static void PrintWelcome()
        {
            Console.Clear();

            Console.Write("+----------------------------------+\n");
            Console.Write("|        SUPER WYSZUKIWARKA        |\n");
            Console.Write("+----------------------------------+\n");
            Console.Write("|               RESET              |\n");
            Console.Write("|             06.04.2016           |\n");
            Console.Write("+----------------------------------+\n\n");
        }
    }
}
