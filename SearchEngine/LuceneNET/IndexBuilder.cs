﻿using Db;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Store;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngine.LuceneNET
{
    public class IndexBuilder
    {
        public static string IndexFolderPath
        {
            get
            {
                return System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "LuceneIndexFolder");
            }
        }

        public event Action BuildIndexCompleted;

        public void BuildIndex()
        {
            if (System.IO.Directory.Exists(IndexFolderPath))
                System.IO.Directory.Delete(IndexFolderPath, true);

            Task.Run(() => Console.WriteLine("*Rozpoczynam budowę indeksu..."));

            var sqlDataList = GetSqlData().ToList();

            using (Lucene.Net.Store.Directory indexDirectory = GetIndexDirectory())
            {
                using (var analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30))
                {
                    using (var indexWriter = new IndexWriter(indexDirectory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))

                    {
                        foreach (var item in sqlDataList)
                        {
                            var doc = new Document();

                            var idField = new Field("id", item.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED);
                            var titleField = new Field("title", item.Title, Field.Store.YES, Field.Index.ANALYZED);
                            var descriptionField = new Field("description", item.Description, Field.Store.NO, Field.Index.ANALYZED);

                            doc.Add(idField);
                            doc.Add(titleField);
                            doc.Add(descriptionField);

                            indexWriter.AddDocument(doc);
                        }

                        indexWriter.Optimize();
                    }
                }
            }

            BuildIndexCompleted?.Invoke();
        }

        public static Directory GetIndexDirectory()
        {
            return FSDirectory.Open(IndexFolderPath);
        }

        public static IEnumerable<Articles> GetSqlData()
        {
            var searchCommand = "SELECT [Id], [Title], [Description] FROM Articles";

            using (var connection = new SqlConnection(DbContext.ConnectionString))
            {
                connection.Open();

                using (var cmd = new SqlCommand(searchCommand, connection))
                {
                    using (var responseReader = cmd.ExecuteReader())
                    {
                        while (responseReader.Read())
                        {
                            var art = new Articles();

                            art.Id = responseReader.GetGuid(0);
                            art.Title = responseReader.GetString(1);
                            art.Description = responseReader.GetString(2);

                            yield return art;
                        }
                    }
                }
            }
        }
    }
}
